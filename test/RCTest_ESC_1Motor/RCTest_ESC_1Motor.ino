#include <Servo.h>
#include <EEPROM.h>

byte last_channel_1, last_channel_2, last_channel_3, last_channel_4;
unsigned long current_time;
unsigned long timer_channel_1;
unsigned long timer_1, timer_2, timer_3, timer_4;
unsigned long receiver_input_channel_1, receiver_input_channel_2, receiver_input_channel_3, receiver_input_channel_4, throttle;
unsigned long esc_1;
unsigned long zero_timer, esc_loop_timer;
byte eeprom_data[36];


Servo ESC1, ESC2, ESC3, ESC4;

void setup() {
  PCICR |= (1 << PCIE0);                              // Activem les interrupcions per el registre PCMSK0.
  PCMSK0 |= (1 << PCINT0);                            // Activem el pin digital 8 perque dispari la interrupco al canviar de valor.
  PCMSK0 |= (1 << PCINT1);                            // Activem el pin digital 9 perque dispari la interrupco al canviar de valor.
  PCMSK0 |= (1 << PCINT2);                            // Activem el pin digital 10 perque dispari la interrupco al canviar de valor.
  PCMSK0 |= (1 << PCINT3);                            // Activem el pin digital 11 perque dispari la interrupco al canviar de valor.
  
  DDRD |= B00010000;                                  // Configurem el pin D4 com a sortida.

  Serial.begin(57600);
  Serial.print("GO");
  zero_timer = micros();
}

void loop() {
  while (zero_timer + 4000 > micros());                         // Freqüència 250Hz.
  zero_timer = micros();

  throttle = receiver_input_channel_3;                          // Llegim el valor d'accelaració RC.
  if (throttle < 1000) 
    throttle = 1000;
  else if (throttle > 2000) 
    throttle = 2000;
    
  esc_1 = throttle;                                              // Definim l'amplada del nostre pols.
  
  PORTD |= B00010000;                                            // Canviem el valor del pin D4 a HIGH.
  
  timer_channel_1 = esc_1 + zero_timer;                          // Calculem el temps quan haurem de canviar el valor del pin D4 a LOW.

  while(PIND & B00010000){                                       // Mentre el valor de D4 és HIGH.
    esc_loop_timer = micros();                                   // Obtenim el temps actual.
    if(timer_channel_1 <= esc_loop_timer) PORTD &= B11101111;    // Quan el temps ha passat canviem el valor del pin D4 a LOW.
  }
}

ISR(PCINT0_vect) {
  current_time = micros();
  
  // Canal 1
  if(PINB & B00000001){                                                     // Si el pin D8 - HIGH
    if(last_channel_1 == 0){                                                // Si abans el pin D8 - LOW
      last_channel_1 = 1;                                                   // Guardem valor actual del pin D8.
      timer_1 = current_time;                                               // Guardem current_time a timer_1.
    }
  }
  else if(last_channel_1 == 1){                                             // Altrament si el pin D8 - LOW i abans estava HIGH
    last_channel_1 = 0;                                                     // Guardem valor actual del pin D8.
    receiver_input_channel_1 = current_time - timer_1;                      // Guardem valor senyal del canal 1.
  }
  
  // Canal 2
  if(PINB & B00000010){                                                     // Si el pin D9 - HIGH
    if(last_channel_2 == 0){                                                // Si abans el pin D9 - LOW
      last_channel_2 = 1;                                                   // Guardem valor actual del pin D9.
      timer_2 = current_time;                                               // Guardem current_time a timer_2.
    }
  }
  else if(last_channel_2 == 1){                                             // Altrament si el pin D9 - LOW i abans estava HIGH
    last_channel_2 = 0;                                                     // Guardem valor actual del pin D9.
    receiver_input_channel_2 = current_time - timer_2;                      // Guardem valor senyal del canal 2.
  }
  
  // Canal 3
  if(PINB & B00000100){                                                     // Si el pin D10 - HIGH
    if(last_channel_3 == 0){                                                // Si abans el pin D10 - LOW
      last_channel_3 = 1;                                                   // Guardem valor actual del pin D10.
      timer_3 = current_time;                                               // Guardem current_time a timer_3.
    }
  }
  else if(last_channel_3 == 1){                                             // Altrament si el pin D10 - LOW i abans estava HIGH
    last_channel_3 = 0;                                                     // Guardem valor actual del pin D10.
    receiver_input_channel_3 = current_time - timer_3;                      // Guardem valor senyal del canal 3.
  }
  
  // Canal 4
  if(PINB & B00001000){                                                     // Si el pin D11 - HIGH
    if(last_channel_4 == 0){                                                // Si abans el pin D11 - LOW
      last_channel_4 = 1;                                                   // Guardem valor actual del pin D11.
      timer_4 = current_time;                                               // Guardem current_time a timer_4.
    }
  }
  else if(last_channel_4 == 1){                                             // Altrament si el pin D11 - LOW i abans estava HIGH
    last_channel_4 = 0;                                                     // Guardem valor actual del pin D11.
    receiver_input_channel_4 = current_time - timer_4;                      // Guardem valor senyal del canal 4.
  }
}
