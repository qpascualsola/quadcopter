#include <LiquidCrystal_I2C.h>

#include "Wire.h"


// Constants
const int CALIB_GYRO_LOOPS = 2000;
const uint8_t MPU6050_ADDR = 0x68;
const uint8_t ACCEL_XOUT_H = 0x3B;

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);                         // Conectem amb l'LCD via I2C.

// Variables
int gyro_data_x, gyro_data_y, gyro_data_z;
double gyro_cal_x, gyro_cal_y, gyro_cal_z;
float angle_pitch_gyr = 0.0, angle_roll_gyr = 0.0;
long acc_data_x, acc_data_y, acc_data_z, acc_total_vector;
float angle_pitch_acc = 0.0, angle_roll_acc = 0.0;
float angle_pitch_output = 0.0, angle_roll_output = 0.0;
int angle_pitch_buffer, angle_roll_buffer;
int lcd_loop_counter;
int temperature;
bool set_gyro_angles = false;

unsigned long zero_timer;

void setup() {
  Wire.begin();
  
  // Serial @ 57600bps 
  Serial.begin(57600);      
  Serial.println("MPU6050 Test"); 
  Serial.println("");

  // Configurem el pin D12 com a output.
  DDRB |= B00010000;  
  
  delay(250);
  Serial.println("Setup IMU...");
  setup_IMU();

  delay(250);
  Serial.println("Setup done !");
  
  // Inicialitzem LCD.
  lcd.init();
  lcd.backlight();

  // Calibrem el giroscopi.
  lcd.setCursor(0,0);
  lcd.print("Calibrating");
  calibrate_gyro();
  lcd.clear();
  lcd.setCursor(0,1);
  lcd.print("Calibration OK");

  // Escriure text al LCD.
  lcd.clear();                                                         
  lcd.setCursor(0,0);                                                  
  lcd.print("Pitch:");                                                 
  lcd.setCursor(0,1);   
  lcd.print("Roll :");
  
  set_gyro_angles = false;
  zero_timer = micros();

  Serial.println("Finish setup");
}

void setup_IMU() {
  uint8_t gyro_address = 0x68;
  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x6B);                                           // Volem escriure al registre PWR_MGMT_1 (0x6B).
  Wire.write(0x00);                                           // Escrivim 00000000 al registre per activar el sensor.
  Wire.endTransmission();                                     // Fi de la comuniació.

  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1B);                                           // Volem escriure al registre GYRO_CONFIG (0x1B). 
  Wire.write(0x08);                                           // Escrivim 00001000 al registre per definir la sensibilitat a 500dps.
  Wire.endTransmission();                                     // Fi de la comuniació.

  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1C);                                           // Volem escriure al registre ACCEL_CONFIG (0x1C). 
  Wire.write(0x10);                                           // Escrivim 00010000 al registre per definir la sensibilitat a +/- 8g.
  Wire.endTransmission();                                     // Fi de la comuniació.

  // Comprovem que les dades s'han guardat correctament.
  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1B);                                           // Volem llegir el registre GYRO_CONFIG (0x1B).
  Wire.endTransmission();                                     // Fi de la comuniació.
  Wire.requestFrom(gyro_address, 1);                          // Demanem 1 byte al sensor.
  while(Wire.available() < 1);                                // Esperem a que tinguem les dades per llegir.
  
  if(Wire.read() != 0x08){                                    // Comprovem que el valor sigui el que hem escrit anteriorment.
    Serial.println("GYRO_CONFIG INCORRECT");
    while(1) delay(10);
  }

  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1A);                                           // Volem escriure al registre CONFIG (0x1A). 
  Wire.write(0x03);                                           // Escrivim 00000011 al registre per definir el Digital Low Filter.
  Wire.endTransmission();                                     // Fi de la comuniació.
}

void calibrate_gyro() {
  int x = 0;
  for (int i = 0; i < CALIB_GYRO_LOOPS; i++) {
    if (i % 125 == 0) {
      lcd.setCursor(x, 1);
      lcd.print(".");
      x++;
    }
    read_IMU_data();
    gyro_cal_x += gyro_data_x;
    gyro_cal_y += gyro_data_y;
    gyro_cal_z += gyro_data_z;
    delay(3);
  }
  
  gyro_cal_x /= CALIB_GYRO_LOOPS;
  gyro_cal_y /= CALIB_GYRO_LOOPS;
  gyro_cal_z /= CALIB_GYRO_LOOPS;
}

void read_IMU_data() {
  Wire.beginTransmission(MPU6050_ADDR);                               // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(ACCEL_XOUT_H);                                           // Volem llegir el registre ACCEL_XOUT_H (0x3B).
  Wire.endTransmission();                                             // Fi de la comuniació.

  Wire.requestFrom(MPU6050_ADDR, 14);                                 // Demanem 14 bytes al sensor.
  while (Wire.available() < 14);                                      // Esperem a que tinguem les dades per llegir.
  
  acc_data_x = (Wire.read() << 8) | Wire.read();
  acc_data_y = (Wire.read() << 8) | Wire.read(); 
  acc_data_z = (Wire.read() << 8) | Wire.read();
  temperature = (Wire.read() << 8) | Wire.read();
  gyro_data_x = (Wire.read() << 8) | Wire.read(); 
  gyro_data_y = (Wire.read() << 8) | Wire.read(); 
  gyro_data_z = (Wire.read() << 8) | Wire.read(); 
}

void loop() {
  while (zero_timer + 4000 > micros());
  zero_timer = micros();

  PORTB |= B00010000;                                                 // Pin D12 - HIGH per l'oscil·loscopi.
  read_IMU_data();                                                    // Llegim les dades de l'IMU

  // Restem les dades de calibració.
  gyro_data_x -= gyro_cal_x;
  gyro_data_y -= gyro_cal_y;
  gyro_data_z -= gyro_cal_z;

  // Càlcul d'angles giroscopi.
  // 0.0000611 = 1 / 250Hz / 65.5.
  angle_pitch_gyr += gyro_data_y * 0.0000611;                                   // Calculem l'angle pitch rotat i el sumem a l'angle_pitch per saber l'orientació.
  angle_roll_gyr += gyro_data_x * 0.0000611;                                    // Calculem l'angle roll rotat i el sumem a l'angle_roll per saber l'orientació.

  //0.000001066 = 0.0000611 * (3.142(PI) / 180degr) La funció sin és en radiants.
  angle_pitch_gyr -= angle_roll_gyr * sin(gyro_data_z * 0.000001066);           // Si la IMU ha rotat en yaw transferim l'angle roll a l'angle pitch.
  angle_roll_gyr += angle_pitch_gyr * sin(gyro_data_z * 0.000001066);           // Si la IMU ha rotat transferim l'angle pitch a l'angle roll.

  // Càlcul d'angles acceleròmetre.
  acc_total_vector = sqrt((acc_data_x*acc_data_x)+(acc_data_y*acc_data_y)+(acc_data_z*acc_data_z));   // Calculem el vector total d'acceleració.
  // 57.296 = 1 / (3.142 / 180) La funció sin és en radiants.
  angle_pitch_acc = asin((float)acc_data_y/acc_total_vector)* 57.296;                                 // Calculem l'angle pitch.
  angle_roll_acc = asin((float)acc_data_x/acc_total_vector)* -57.296;                                 // Calculem l'angle roll.

  if (set_gyro_angles) {
    angle_pitch_gyr = angle_pitch_gyr * 0.9996 + angle_pitch_acc * 0.0004;     // Corregim el drift de l'angle pitch del giroscopi amb l'angle pitch de l'accelerometre.
    angle_roll_gyr = angle_roll_gyr * 0.9996 + angle_roll_acc * 0.0004;        // Corregim el drift de l'angle roll del giroscopi amb l'angle roll de l'accelerometre.
    angle_pitch_output = angle_pitch_gyr;                                      // Assignem angle pitch.
    angle_roll_output = angle_roll_gyr;                                        // Assignem angle roll.
  } 
  else {
    angle_pitch_gyr = angle_pitch_acc;                                         // El primer cop assignem l'angle pitch de l'accelerometre com a inicial.
    angle_roll_gyr = angle_roll_acc;                                           // El primer cop assignem l'angle roll de l'accelerometre com a inicial.
    angle_pitch_output = angle_pitch_acc;                                      // El primer cop assignem l'angle pitch de l'accelerometre com a inicial.
    angle_roll_output = angle_roll_acc;                                        // El primer cop assignem l'angle roll de l'accelerometre com a inicial.
    set_gyro_angles = true;
  }
  
  PORTB &= B11101111;                                                          // Pin D12 - LOW per l'oscil·loscopi.

  write_LCD();                                                                 // Escrivim els angles a la LCD.
}

void write_LCD(){                                                      
  // Per aconseguir un programa que vagi a una freqüència de 250Hz hem d'escriure només un caràcter a cada loop.
  // Escriure multiples caràcters comporta massa temps.
  if (lcd_loop_counter == 14) lcd_loop_counter = 0;                       // Reset al contador després de 14 caràcters.
  lcd_loop_counter ++;                                                    // Incrementem el contador.
  if (lcd_loop_counter == 1) {
    angle_pitch_buffer = angle_pitch_output * 10;                         // Buffer de l'angle ja que canviarà.
    lcd.setCursor(6,0);                                                   // Movem el cursor LCD.
  }
  if (lcd_loop_counter == 2){
    if (angle_pitch_buffer < 0) lcd.print("-");                           // Si l'angle és inferior a 0 - Print - 
    else lcd.print("+");                                                  // Si l'angle és superior a 0 - Print + 
  }
  if (lcd_loop_counter == 3) lcd.print(abs(angle_pitch_buffer)/1000);     // Print primer número.
  if (lcd_loop_counter == 4) lcd.print((abs(angle_pitch_buffer)/100)%10); // Print segon número.
  if (lcd_loop_counter == 5) lcd.print((abs(angle_pitch_buffer)/10)%10);  // Print tercer número.
  if (lcd_loop_counter == 6) lcd.print(".");                              // Print quart número.
  if (lcd_loop_counter == 7) lcd.print(abs(angle_pitch_buffer)%10);       // Print cinque número.

  if (lcd_loop_counter == 8) {
    angle_roll_buffer = angle_roll_output * 10;
    lcd.setCursor(6,1);
  }
  if (lcd_loop_counter == 9) {
    if (angle_roll_buffer < 0) lcd.print("-");                            // Si l'angle és inferior a 0 - Print - 
    else lcd.print("+");                                                  // Si l'angle és superior a 0 - Print + 
  }
  if (lcd_loop_counter == 10) lcd.print(abs(angle_roll_buffer)/1000);     // Print primer número.
  if (lcd_loop_counter == 11) lcd.print((abs(angle_roll_buffer)/100)%10); // Print segon número.
  if (lcd_loop_counter == 12) lcd.print((abs(angle_roll_buffer)/10)%10);  // Print tercer número.
  if (lcd_loop_counter == 13) lcd.print(".");                             // Print quart número.
  if (lcd_loop_counter == 14) lcd.print(abs(angle_roll_buffer)%10);       // Print cinque número.
}
