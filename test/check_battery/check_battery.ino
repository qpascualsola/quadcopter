const int a0 = A0;
const int led = 5;

float volt;

void setup() {
  pinMode(a0, INPUT);
  pinMode(led, OUTPUT);

  Serial.begin(9600);
}

void check_battery() {
  float ar = analogRead(a0);
  Serial.println(ar);

  if (ar > 0) {
    volt = (volt * 0.7) + ((ar * (5.00 / 1023.00) * 3.20) * 0.3);
  }
  else volt = 0;
}

void loop() {
  check_battery();
  Serial.println(volt, 4);
  Serial.println();
  
  delay(20);
}
