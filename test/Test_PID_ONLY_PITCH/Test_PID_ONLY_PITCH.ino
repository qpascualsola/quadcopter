#include <LiquidCrystal_I2C.h>
#include <EEPROM.h>
#include "Wire.h"

///////////////////////////////////////////////////////////////////////////////////////
// CONSTANTS
///////////////////////////////////////////////////////////////////////////////////////
const int CALIB_GYRO_LOOPS = 2000;
const uint8_t MPU6050_ADDR  = 0x68;
const uint8_t ACCEL_XOUT_H   = 0x3B;

///////////////////////////////////////////////////////////////////////////////////////
// PID - CONSTANTS
///////////////////////////////////////////////////////////////////////////////////////
float pid_p_gain_pitch = 1;                 // Constant P per PID.
float pid_i_gain_pitch = 0;                 // Constant I per PID.
float pid_d_gain_pitch = 0;                 // Constant D per PID.
int pid_max_pitch = 400;                    // Valor màxim del PID.


///////////////////////////////////////////////////////////////////////////////////////
// PID - VARIABLES
///////////////////////////////////////////////////////////////////////////////////////
float pid_error_temp;
float pitch_level_adjust, gyro_pitch_input;
float pid_pitch_setpoint, pid_i_mem_pitch, pid_output_pitch, pid_last_pitch_d_error;

///////////////////////////////////////////////////////////////////////////////////////
// IMU - VARIABLES
///////////////////////////////////////////////////////////////////////////////////////
int gyro_data_x, gyro_data_y, gyro_data_z;
double gyro_cal_x, gyro_cal_y, gyro_cal_z;
float angle_pitch_gyr = 0.0, angle_roll_gyr = 0.0;
long acc_data_x, acc_data_y, acc_data_z, acc_total_vector;
float angle_pitch_acc = 0.0, angle_roll_acc = 0.0;
float angle_pitch_output = 0.0, angle_roll_output = 0.0;
bool set_gyro_angles = false;
int angle_pitch_buffer, angle_roll_buffer;

///////////////////////////////////////////////////////////////////////////////////////
// RC - VARIABLES
///////////////////////////////////////////////////////////////////////////////////////
unsigned long current_time;
unsigned long last_channel_3, last_channel_2, timer_3, timer_2;
unsigned long receiver_input_channel_3, receiver_input_channel_2, throttle, receiver_pitch;
unsigned long timer_esc_1, timer_esc_2;
unsigned long esc_1, esc_2;
unsigned long esc_timer, esc_loop_timer;

///////////////////////////////////////////////////////////////////////////////////////
// COMMON - VARIABLES
///////////////////////////////////////////////////////////////////////////////////////
int lcd_loop_counter;
int temperature;
unsigned long zero_timer;
byte eeprom_data[36];

void setup() {
  Wire.begin();                                                             // Comencem la comunicació I2C com a "master".
  TWBR = 12;                                                                // Modifiquem la velocitat de rellotge del I2C a 400kHz.
  
  // Serial @ 57600bps 
  Serial.begin(57600);      
  Serial.println("PID Only Pitch Test"); 
  Serial.println("");

  DDRD |= B11111100;                                                        // Configurem els pins digitals 2, 3, 4, 5 (per leds) 6, 7 (per ESC) com output.
  DDRB |= B00010000;                                                        // Configurem el pin digital 12 (per oscil·loscopi) com output.

  Serial.println("Test leds (left to right)...");
  PORTD &= B11000011;
  delay(500);
  for (int i = 0; i < 4; i++) {
    if (i == 3) PORTD |= B00000100;
    if (i == 2) PORTD |= B00001000;
    if (i == 1) PORTD |= B00010000;
    if (i == 0) PORTD |= B00100000;
    Serial.print(i);
    Serial.print(" ");
    
    // Per tal que els ESC no pitin enviem un pols de 1ms.
    PORTD |= B11000000;                                                     // Pin D6, D7 - HIGH.
    delayMicroseconds(1000);                                                // Esperem 1ms.
    PORTD &= B00111111;                                                     // Pin D6, D7 - LOW.
    delayMicroseconds(3000);                                                // Esperem 4ms.
  }
  PORTD &= B11000011;
  Serial.println("Leds OFF");

  PCICR |= (1 << PCIE0);                                                    // Activem les interrupcions per el registre PCMSK0.
  PCMSK0 |= (1 << PCINT2);                                                  // Activem el pin digital 10 perque dispari la interrupco al canviar de valor.

  // Llegim la memòria EEPROM per accés més ràpid.
  for(int i = 0; i <= 35; i++) eeprom_data[i] = EEPROM.read(i);
  
  Serial.println("Setup MPU-6050...");
  setup_IMU();
  Serial.println("Setup done !");

  // Calibrem el giroscopi.
  Serial.print("Calibrating");
  calibrate_gyro();
  Serial.println("OK");
  
  set_gyro_angles = false;
  zero_timer = micros();

  Serial.println("Finish setup");
}

void setup_IMU() {
  uint8_t gyro_address = 0x68;
  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x6B);                                           // Volem escriure al registre PWR_MGMT_1 (0x6B).
  Wire.write(0x00);                                           // Escrivim 00000000 al registre per activar el sensor.
  Wire.endTransmission();                                     // Fi de la comuniació.

  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1B);                                           // Volem escriure al registre GYRO_CONFIG (0x1B). 
  Wire.write(0x08);                                           // Escrivim 00001000 al registre per definir la sensibilitat a 500dps.
  Wire.endTransmission();                                     // Fi de la comuniació.

  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1C);                                           // Volem escriure al registre ACCEL_CONFIG (0x1C). 
  Wire.write(0x10);                                           // Escrivim 00010000 al registre per definir la sensibilitat a +/- 8g.
  Wire.endTransmission();                                     // Fi de la comuniació.

  // Comprovem que les dades s'han guardat correctament.
  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1B);                                           // Volem llegir el registre GYRO_CONFIG (0x1B).
  Wire.endTransmission();                                     // Fi de la comuniació.
  Wire.requestFrom(gyro_address, 1);                          // Demanem 1 byte al sensor.
  while(Wire.available() < 1);                                // Esperem a que tinguem les dades per llegir.
  
  if(Wire.read() != 0x08){                                    // Comprovem que el valor sigui el que hem escrit anteriorment.
    Serial.println("GYRO_CONFIG INCORRECT");
    while(1) delay(10);
  }

  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1A);                                           // Volem escriure al registre CONFIG (0x1A). 
  Wire.write(0x03);                                           // Escrivim 00000011 al registre per definir el Digital Low Filter.
  Wire.endTransmission();                                     // Fi de la comuniació.
}

void calibrate_gyro() {
  int x = 0;
  for (int i = 0; i < CALIB_GYRO_LOOPS; i++) {
    if (i % 125 == 0) {
      Serial.print(".");
      x++;
    }
    read_IMU_data();
    gyro_cal_x += gyro_data_x;
    gyro_cal_y += gyro_data_y;
    gyro_cal_z += gyro_data_z;

    // Per tal que els ESC no pitin enviem un pols de 1ms.
    PORTD |= B11000000;                                                     // Pin D6, D7 - HIGH.
    delayMicroseconds(1000);                                                // Esperem 1ms.
    PORTD &= B00111111;                                                     // Pin D6, D7 - LOW.
    delayMicroseconds(3000);                                                // Esperem 4ms.
  }
  
  gyro_cal_x /= CALIB_GYRO_LOOPS;
  gyro_cal_y /= CALIB_GYRO_LOOPS;
  gyro_cal_z /= CALIB_GYRO_LOOPS;
}

void read_IMU_data() {
  Wire.beginTransmission(MPU6050_ADDR);                               // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(ACCEL_XOUT_H);                                           // Volem llegir el registre ACCEL_XOUT_H (0x3B).
  Wire.endTransmission();                                             // Fi de la comuniació.

  Wire.requestFrom(MPU6050_ADDR, 14);                                 // Demanem 14 bytes al sensor.
  while (Wire.available() < 14);                                      // Esperem a que tinguem les dades per llegir.
  
  acc_data_x = (Wire.read() << 8) | Wire.read();
  acc_data_y = (Wire.read() << 8) | Wire.read(); 
  acc_data_z = (Wire.read() << 8) | Wire.read();
  temperature = (Wire.read() << 8) | Wire.read();
  gyro_data_x = (Wire.read() << 8) | Wire.read(); 
  gyro_data_y = (Wire.read() << 8) | Wire.read(); 
  gyro_data_z = (Wire.read() << 8) | Wire.read(); 
}

void calculate_angles() {
  // Restem les dades de calibració.
  gyro_data_x -= gyro_cal_x;
  gyro_data_y -= gyro_cal_y;
  gyro_data_z -= gyro_cal_z;

  // Càlcul d'angles giroscopi.
  // 0.0000611 = 1 / 250Hz / 65.5.
  angle_pitch_gyr += gyro_data_y * 0.0000611;                                   // Calculem l'angle pitch rotat i el sumem a l'angle_pitch per saber l'orientació.
  angle_roll_gyr += gyro_data_x * 0.0000611;                                    // Calculem l'angle roll rotat i el sumem a l'angle_roll per saber l'orientació.

  //0.000001066 = 0.0000611 * (3.142(PI) / 180degr) La funció sin és en radiants.
  angle_pitch_gyr -= angle_roll_gyr * sin(gyro_data_z * 0.000001066);           // Si la IMU ha rotat en yaw transferim l'angle roll a l'angle pitch.
  angle_roll_gyr += angle_pitch_gyr * sin(gyro_data_z * 0.000001066);           // Si la IMU ha rotat transferim l'angle pitch a l'angle roll.

  // Càlcul d'angles acceleròmetre.
  acc_total_vector = sqrt((acc_data_x*acc_data_x)+(acc_data_y*acc_data_y)+(acc_data_z*acc_data_z));   // Calculem el vector total d'acceleració.
  // 57.296 = 1 / (3.142 / 180) La funció sin és en radiants.
  angle_pitch_acc = asin((float)acc_data_y/acc_total_vector)* 57.296;                                 // Calculem l'angle pitch.
  angle_roll_acc = asin((float)acc_data_x/acc_total_vector)* -57.296;                                 // Calculem l'angle roll.

  if (set_gyro_angles) {
    angle_pitch_gyr = angle_pitch_gyr * 0.9996 + angle_pitch_acc * 0.0004;     // Corregim el drift de l'angle pitch del giroscopi amb l'angle pitch de l'accelerometre.
    angle_roll_gyr = angle_roll_gyr * 0.9996 + angle_roll_acc * 0.0004;        // Corregim el drift de l'angle roll del giroscopi amb l'angle roll de l'accelerometre.
    angle_pitch_output = angle_pitch_gyr;                                      // Assignem angle pitch.
    angle_roll_output = angle_roll_gyr;                                        // Assignem angle roll.
  } 
  else {
    angle_pitch_gyr = angle_pitch_acc;                                         // El primer cop assignem l'angle pitch de l'accelerometre com a inicial.
    angle_roll_gyr = angle_roll_acc;                                           // El primer cop assignem l'angle roll de l'accelerometre com a inicial.
    angle_pitch_output = angle_pitch_acc;                                      // El primer cop assignem l'angle pitch de l'accelerometre com a inicial.
    angle_roll_output = angle_roll_acc;                                        // El primer cop assignem l'angle roll de l'accelerometre com a inicial.
    set_gyro_angles = true;
  }
}

void calculate_pid() {
  pid_error_temp = gyro_pitch_input - pid_pitch_setpoint;
  pid_i_mem_pitch += pid_i_gain_pitch * pid_error_temp;
  if(pid_i_mem_pitch > pid_max_pitch) pid_i_mem_pitch = pid_max_pitch;
  else if(pid_i_mem_pitch < pid_max_pitch * -1) pid_i_mem_pitch = pid_max_pitch * -1;

  pid_output_pitch = pid_p_gain_pitch * pid_error_temp + pid_i_mem_pitch + pid_d_gain_pitch * (pid_error_temp - pid_last_pitch_d_error);
  if(pid_output_pitch > pid_max_pitch) pid_output_pitch = pid_max_pitch;
  else if(pid_output_pitch < pid_max_pitch * -1) pid_output_pitch = pid_max_pitch * -1;

  pid_last_pitch_d_error = pid_error_temp;
}

void loop() {
  while (zero_timer + 4000 > micros());
  zero_timer = micros();

  PORTB |= B00010000;

  read_IMU_data();                                                                  // Llegim les dades de l'IMU.
  calculate_angles();                                                               // Calculem els angles.
  
  gyro_pitch_input = (gyro_pitch_input * 0.7) + ((gyro_data_x / 65.5) * 0.3);       // Pitch input en dps aplicant un filtre complementari per reduir soroll.
  pitch_level_adjust = angle_pitch_output * 15;                                     // Calculem l'ajustament per compensar la inclinació en pitch.

  // El setpoint del PID estarà en dps ja que volem que depengui del pitch rebut del comandament RC.
  // Dividim per 3.0 per ajustar que la velocitat màxima de rotació quan posem el joystick a un extrem sigui de 164dps ((500-8)/3 = 164dps).
  // Si volguessim poder girar més ràpid o més lent hauriem de modficar aquest 3.
  pid_pitch_setpoint = 0;
  if (receiver_pitch > 1508) pid_pitch_setpoint = receiver_pitch - 1508;
  else if (receiver_pitch < 1492) pid_pitch_setpoint = receiver_pitch - 1492;
  pid_pitch_setpoint -= pitch_level_adjust;                                         // Restem l'ajustament de l'angle al setpoint.
  pid_pitch_setpoint /= 3.0;                                                        // Dividim per 3 per ajustar la velocitat angular màxima.

  calculate_pid();                                                                  // Calculem el PID.

  throttle = convert_receiver_channel(3);                                           // Agafem la senyal RC.
  if (throttle > 1600) throttle = 1600;                                             // Posem un límit a la senyal.
  esc_1 = throttle - pid_output_pitch;                                              // Correcció de l'ESC 1.
  esc_2 = throttle + pid_output_pitch;                                              // Correcció de l'ESC 2.
  if (esc_1 < 1100) esc_1 = 1100;
  if (esc_2 < 1100) esc_2 = 1100;
  if (esc_1 > 2000) esc_1 = 2000;
  if (esc_2 > 2000) esc_2 = 2000;
  
  esc_timer = micros();
  PORTD |= B11000000;                                                               // Pins D6 i D7 - HIGH per començar la senyal PWM.
  timer_esc_1 = esc_1 + esc_timer;                                                  // Calculem el temps final per l'ESC 1.
  timer_esc_2 = esc_2 + esc_timer;                                                  // Calculem el temps final per l'ESC 2.

  Serial.println(esc_1);
  Serial.println(esc_2);
  Serial.println();

  long diff = esc_1 - esc_2;

  // Indicacions amb leds.
  if (diff > 150) PORTD |= B00100000;
  else PORTD &= B11011111;
  if (diff > 75) PORTD |= B00010000;
  else PORTD &= B11101111;
  if (diff < (-75)) PORTD |= B00001000;
  else PORTD &= B11110111;
  if (diff < (-150)) PORTD |= B00000100;
  else PORTD &= B11111011;

  while (PORTD >= 64) {                                                            // Mentre els ports D6 o D7 estiguin a HIGH.
    esc_loop_timer = micros();                                                     // Comprovem el temps actual.
    if(timer_esc_1 <= esc_loop_timer) PORTD &= B10111111;                           // Quan el temps del ESC 3 ha finalitzat: D6 - LOW.
    if(timer_esc_2 <= esc_loop_timer) PORTD &= B01111111;                           // Quan el temps del ESC 4 ha finalitzat: D7 - LOW.
  }

  PORTB &= B11101111;
}

int convert_receiver_channel(int channel){
  int low, center, high, actual;
  int difference;

  actual = receiver_input_channel_3;
                                        
  low = (eeprom_data[channel * 2 + 15] << 8) | eeprom_data[channel * 2 + 14];     // Guardem el valor mínim del canal que volem calibrar.
  center = (eeprom_data[channel * 2 - 1] << 8) | eeprom_data[channel * 2 - 2];    // Guardem el valor mig del canal que volem calibrar.
  high = (eeprom_data[channel * 2 + 7] << 8) | eeprom_data[channel * 2 + 6];      // Guardem el valor màxim del canal que volem calibrar.

  if (actual < center){                                                           // El valor actual és més petit que el mig.
    if (actual < low) actual = low;                                               // Si és més petit que el mínim guardem el màxim.
    difference = ((long)(center - actual) * (long)500) / (center - low);          // Projectem el valor a 1000 - 2000 microsegons.
    return 1500 - difference;                                                     
  }
  else if (actual > center){                                                      // El valor actual és més gran que el mig.
    if (actual > high) actual = high;                                             // Si és més gran que el màxim guardem el mínim.
    difference = ((long)(actual - center) * (long)500) / (high - center);         // Projectem el valor a 1000 - 2000 microsegons.
    return 1500 + difference;                                                     
  }
  else return 1500;                                                               // Retornem el punt mig.
}

ISR(PCINT0_vect) {
  current_time = micros();
  
  // Canal 2
  if(PINB & B00000010){                                                     // Si el pin D9 - HIGH
    if(last_channel_2 == 0){                                                // Si abans el pin D9 - LOW
      last_channel_2 = 1;                                                   // Guardem valor actual del pin D9.
      timer_2 = current_time;                                               // Guardem current_time a timer_2.
    }
  }
  else if(last_channel_2 == 1){                                             // Altrament si el pin D9 - LOW i abans estava HIGH
    last_channel_2 = 0;                                                     // Guardem valor actual del pin D9.
    receiver_input_channel_2 = current_time - timer_2;                      // Guardem valor senyal del canal 2.
  }
  
  // Canal 3
  if(PINB & B00000100){                                                     // Si el pin D10 - HIGH
    if(last_channel_3 == 0){                                                // Si abans el pin D10 - LOW
      last_channel_3 = 1;                                                   // Guardem valor actual del pin D10.
      timer_3 = current_time;                                               // Guardem current_time a timer_3.
    }
  }
  else if(last_channel_3 == 1){                                             // Altrament si el pin D10 - LOW i abans estava HIGH
    last_channel_3 = 0;                                                     // Guardem valor actual del pin D10.
    receiver_input_channel_3 = current_time - timer_3;                      // Guardem valor senyal del canal 3.
  }
}
