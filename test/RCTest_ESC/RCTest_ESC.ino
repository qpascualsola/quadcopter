#include <Servo.h>
#include <EEPROM.h>

byte last_channel_1, last_channel_2, last_channel_3, last_channel_4;
unsigned long current_time;
unsigned long timer_channel_1, timer_channel_2, timer_channel_3, timer_channel_4;
unsigned long timer_1, timer_2, timer_3, timer_4;
unsigned long receiver_input_channel_1, receiver_input_channel_2, receiver_input_channel_3, receiver_input_channel_4, throttle;
unsigned long esc_1, esc_2, esc_3, esc_4;
unsigned long zero_timer, esc_loop_timer;
byte eeprom_data[36];


Servo ESC1, ESC2, ESC3, ESC4;

void setup() {
  PCICR |= (1 << PCIE0);                              // Activem les interrupcions per el registre PCMSK0.
  PCMSK0 |= (1 << PCINT0);                            // Activem el pin digital 8 perque dispari la interrupco al canviar de valor.
  PCMSK0 |= (1 << PCINT1);                            // Activem el pin digital 9 perque dispari la interrupco al canviar de valor.
  PCMSK0 |= (1 << PCINT2);                            // Activem el pin digital 10 perque dispari la interrupco al canviar de valor.
  PCMSK0 |= (1 << PCINT3);                            // Activem el pin digital 11 perque dispari la interrupco al canviar de valor.
  
  DDRD |= B11110000;                                  // Configurem els pins D4, D5, D6 i D7 com a sortida.

  Serial.begin(57600);
  Serial.print("GO");
  zero_timer = micros();
}

void loop() {
  while (zero_timer + 4000 > micros());
  zero_timer = micros();

  throttle = receiver_input_channel_3;
  if (throttle < 1000) 
    throttle = 1000;
    
  esc_1 = throttle;                                              // Definim l'amplada del nostre pols.
  esc_2 = throttle;
  esc_3 = throttle;
  esc_4 = throttle;
  
  PORTD |= B11110000;                                            // Posem els pins D4, D5, D6, D7 a HIGH per començar a enviar la senyal.
  
  timer_channel_1 = esc_1 + zero_timer;                          // Calculem el temps final per l'ESC 1.
  timer_channel_2 = esc_2 + zero_timer;                          // Calculem el temps final per l'ESC 2.
  timer_channel_3 = esc_3 + zero_timer;                          // Calculem el temps final per l'ESC 3.
  timer_channel_4 = esc_4 + zero_timer;                          // Calculem el temps final per l'ESC 4.

  while(PORTD >= 16){                                            // Mentre els ports D4, D5, D6 o D7 estiguin a HIGH.
    esc_loop_timer = micros();                                   // Comprovem el temps actual.
    if(timer_channel_1 <= esc_loop_timer)PORTD &= B11101111;     // Quan el temps del ESC 1 ha finalitzat: D4 - LOW.
    if(timer_channel_2 <= esc_loop_timer)PORTD &= B11011111;     // Quan el temps del ESC 2 ha finalitzat: D5 - LOW.
    if(timer_channel_3 <= esc_loop_timer)PORTD &= B10111111;     // Quan el temps del ESC 3 ha finalitzat: D6 - LOW.
    if(timer_channel_4 <= esc_loop_timer)PORTD &= B01111111;     // Quan el temps del ESC 4 ha finalitzat: D7 - LOW.
  }
}

ISR(PCINT0_vect) {
  current_time = micros();
  
  // Canal 1
  if(PINB & B00000001){                                                     // Si el pin D8 - HIGH
    if(last_channel_1 == 0){                                                // Si abans el pin D8 - LOW
      last_channel_1 = 1;                                                   // Guardem valor actual del pin D8.
      timer_1 = current_time;                                               // Guardem current_time a timer_1.
    }
  }
  else if(last_channel_1 == 1){                                             // Altrament si el pin D8 - LOW i abans estava HIGH
    last_channel_1 = 0;                                                     // Guardem valor actual del pin D8.
    receiver_input_channel_1 = current_time - timer_1;                      // Guardem valor senyal del canal 1.
  }
  
  // Canal 2
  if(PINB & B00000010){                                                     // Si el pin D9 - HIGH
    if(last_channel_2 == 0){                                                // Si abans el pin D9 - LOW
      last_channel_2 = 1;                                                   // Guardem valor actual del pin D9.
      timer_2 = current_time;                                               // Guardem current_time a timer_2.
    }
  }
  else if(last_channel_2 == 1){                                             // Altrament si el pin D9 - LOW i abans estava HIGH
    last_channel_2 = 0;                                                     // Guardem valor actual del pin D9.
    receiver_input_channel_2 = current_time - timer_2;                      // Guardem valor senyal del canal 2.
  }
  
  // Canal 3
  if(PINB & B00000100){                                                     // Si el pin D10 - HIGH
    if(last_channel_3 == 0){                                                // Si abans el pin D10 - LOW
      last_channel_3 = 1;                                                   // Guardem valor actual del pin D10.
      timer_3 = current_time;                                               // Guardem current_time a timer_3.
    }
  }
  else if(last_channel_3 == 1){                                             // Altrament si el pin D9 - LOW i abans estava HIGH
    last_channel_3 = 0;                                                     // Guardem valor actual del pin D9.
    receiver_input_channel_3 = current_time - timer_3;                      // Guardem valor senyal del canal 3.
  }
  
  // Canal 4
  if(PINB & B00001000){                                                     // Si el pin D11 - HIGH
    if(last_channel_4 == 0){                                                // Si abans el pin D11 - LOW
      last_channel_4 = 1;                                                   // Guardem valor actual del pin D11.
      timer_4 = current_time;                                               // Guardem current_time a timer_4.
    }
  }
  else if(last_channel_4 == 1){                                             // Altrament si el pin D11 - LOW i abans estava HIGH
    last_channel_4 = 0;                                                     // Guardem valor actual del pin D11.
    receiver_input_channel_4 = current_time - timer_4;                      // Guardem valor senyal del canal 4.
  }
}
