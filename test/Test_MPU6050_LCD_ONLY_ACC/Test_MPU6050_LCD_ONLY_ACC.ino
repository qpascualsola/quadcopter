#include <LiquidCrystal_I2C.h>

#include "Wire.h"


// Constants
const uint8_t MPU6050_ADDR  = 0x68;
const uint8_t ACCEL_XOUT_H   = 0x3B;

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);                     // Conectem amb l'LCD via I2C.

// Variables
long acc_data_x = 0, acc_data_y = 0, acc_data_z = 0, acc_total_vector;
float angle_pitch_acc = 0.0, angle_roll_acc = 0.0;
float angle_pitch_output, angle_roll_output;
int angle_pitch_buffer, angle_roll_buffer;
int lcd_loop_counter;

unsigned long zero_timer;

void setup() {
  Wire.begin();
  
  // Serial @ 57600bps 
  Serial.begin(57600);      
  Serial.println("MPU6050 Test"); 
  Serial.println("");

  // Configure digital port 4 as output.
  DDRD |= B00010000;  
  
  delay(250);
  Serial.println("Setup gyro...");
  setup_IMU();

  delay(250);
  Serial.println("Setup done !");
  
  // Inicialitzem LCD.
  lcd.init();
  lcd.backlight();
  
  // Escriure text al LCD.
  lcd.clear();                                                         
  lcd.setCursor(0,0);                                                  
  lcd.print("Pitch:");                                                 
  lcd.setCursor(0,1);   
  lcd.print("Roll :");                                               

  zero_timer = micros();

  Serial.println("Finish setup");
}

void setup_IMU() {
  uint8_t gyro_address = 0x68;
  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x6B);                                           // Volem escriure al registre PWR_MGMT_1 (0x6B).
  Wire.write(0x00);                                           // Escrivim 00000000 al registre per activar el sensor.
  Wire.endTransmission();                                     // Fi de la comuniació.

  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1C);                                           // Volem escriure al registre ACCEL_CONFIG (0x1C). 
  Wire.write(0x10);                                           // Escrivim 00010000 al registre per definir la sensibilitat a +/- 8g.
  Wire.endTransmission();                                     // Fi de la comuniació.
  
  // Comprovem que les dades s'han guardat correctament.
  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1C);                                           // Volem llegir el registre ACCEL_CONFIG (0x1C). 
  Wire.endTransmission();                                     // Fi de la comuniació.
  Wire.requestFrom(gyro_address, 1);                          // Demanem 1 byte al sensor.
  while(Wire.available() < 1);                                // Esperem a que tinguem les dades per llegir.

  if(Wire.read() != 0x10){                                    // Comprovem que el valor sigui el que hem escrit anteriorment.
    Serial.println("ACCEL_CONFIG INCORRECT");
    while(1) delay(10);
  }

  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1A);                                           // Volem escriure al registre CONFIG (0x1A). 
  Wire.write(0x03);                                           // Escrivim 00000011 al registre per definir el Digital Low Filter.
  Wire.endTransmission();                                     // Fi de la comuniació.
}

void read_acc_data() {
  Wire.beginTransmission(MPU6050_ADDR);                               // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(ACCEL_XOUT_H);                                           // Volem llegir el registre ACCEL_XOUT_H (0x3B).
  Wire.endTransmission();                                             // Fi de la comuniació.

  Wire.requestFrom(MPU6050_ADDR, 6);                                  // Demanem 6 bytes al sensor.
  while (Wire.available() < 6);                                       // Esperem a que tinguem les dades per llegir.
  
  acc_data_x = (Wire.read() << 8) | Wire.read(); 
  acc_data_y = (Wire.read() << 8) | Wire.read(); 
  acc_data_z = (Wire.read() << 8) | Wire.read(); 
}

void loop() {
  while (zero_timer + 4000 > micros());
  zero_timer = micros();

  PORTD |= B00010000;                                                                                 // Pin D4 - HIGH per l'oscil·loscopi.
  read_acc_ata();                                                                                     // Llegim les dades de l'IMU.

  // Càlcul d'angles acceleròmetre.
  acc_total_vector = sqrt((acc_data_x*acc_data_x)+(acc_data_y*acc_data_y)+(acc_data_z*acc_data_z));   // Calculem el vector total d'acceleració.
  // 57.296 = 1 / (3.142 / 180) La funció sin és en radiants.
  angle_pitch_acc = asin((float)acc_data_y/acc_total_vector)* 57.296;                                 // Calculem l'angle pitch.
  angle_roll_acc = asin((float)acc_data_x/acc_total_vector)* -57.296;                                 // Calculem l'angle roll.

  angle_pitch_output = angle_pitch_acc;                                                               // Assignem angle pitch.
  angle_roll_output = angle_roll_acc;                                                                 // Assignem angle roll.

  write_LCD();                                                                                        // Escrivim els angles a la LCD.
  PORTD &= B11101111;                                                                                 // Pin D12 - LOW per l'oscil·loscopi.
}

void write_LCD(){                                                      
  // Per aconseguir un programa que vagi a una freqüència de 250Hz hem d'escriure només un caràcter a cada loop.
  // Escriure multiples caràcters comporta massa temps.
  if (lcd_loop_counter == 14) lcd_loop_counter = 0;                       // Reset al contador després de 14 caràcters.
  lcd_loop_counter ++;                                                    // Incrementem el contador.
  if (lcd_loop_counter == 1) {
    angle_pitch_buffer = angle_pitch_output * 10;                         // Buffer de l'angle ja que canviarà.
    lcd.setCursor(6,0);                                                   // Movem el cursor LCD.
  }
  if (lcd_loop_counter == 2){
    if (angle_pitch_buffer < 0) lcd.print("-");                           // Si l'angle és inferior a 0 - Print - 
    else lcd.print("+");                                                  // Si l'angle és superior a 0 - Print + 
  }
  if (lcd_loop_counter == 3) lcd.print(abs(angle_pitch_buffer)/1000);     // Print primer número.
  if (lcd_loop_counter == 4) lcd.print((abs(angle_pitch_buffer)/100)%10); // Print segon número.
  if (lcd_loop_counter == 5) lcd.print((abs(angle_pitch_buffer)/10)%10);  // Print tercer número.
  if (lcd_loop_counter == 6) lcd.print(".");                              // Print quart número.
  if (lcd_loop_counter == 7) lcd.print(abs(angle_pitch_buffer)%10);       // Print cinque número.

  if (lcd_loop_counter == 8) {
    angle_roll_buffer = angle_roll_output * 10;
    lcd.setCursor(6,1);
  }
  if (lcd_loop_counter == 9) {
    if (angle_roll_buffer < 0) lcd.print("-");                            // Si l'angle és inferior a 0 - Print - 
    else lcd.print("+");                                                  // Si l'angle és superior a 0 - Print + 
  }
  if (lcd_loop_counter == 10) lcd.print(abs(angle_roll_buffer)/1000);     // Print primer número.
  if (lcd_loop_counter == 11) lcd.print((abs(angle_roll_buffer)/100)%10); // Print segon número.
  if (lcd_loop_counter == 12) lcd.print((abs(angle_roll_buffer)/10)%10);  // Print tercer número.
  if (lcd_loop_counter == 13) lcd.print(".");                             // Print quart número.
  if (lcd_loop_counter == 14) lcd.print(abs(angle_roll_buffer)%10);       // Print cinque número.
}
