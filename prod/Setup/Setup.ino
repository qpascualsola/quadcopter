#include <EEPROM.h>

unsigned long current_time;

int center_channel_1, center_channel_2, center_channel_3, center_channel_4;
int high_channel_1, high_channel_2, high_channel_3, high_channel_4;
int low_channel_1, low_channel_2, low_channel_3, low_channel_4;

byte last_channel_1, last_channel_2, last_channel_3, last_channel_4;
unsigned long timer_1, timer_2, timer_3, timer_4;
unsigned long receiver_input_channel_1, receiver_input_channel_2, receiver_input_channel_3, receiver_input_channel_4;

void setup() {
  Serial.begin(57600);
  delay(250);
  Serial.println("Init setup.");

  
  PCICR |= (1 << PCIE0);                              // Activem les interrupcions per el registre PCMSK0.
  PCMSK0 |= (1 << PCINT0);                            // Activem el pin digital 8 perque dispari la interrupco al canviar de valor.
  PCMSK0 |= (1 << PCINT1);                            // Activem el pin digital 9 perque dispari la interrupco al canviar de valor.
  PCMSK0 |= (1 << PCINT2);                            // Activem el pin digital 10 perque dispari la interrupco al canviar de valor.
  PCMSK0 |= (1 << PCINT3);                            // Activem el pin digital 11 perque dispari la interrupco al canviar de valor.

  Serial.println("Col·loca els joysticks en posicio central.");
  delay(5000);

  // Guardem la posició central.
  center_channel_1 = receiver_input_channel_1;  
  center_channel_2 = receiver_input_channel_2;
  center_channel_3 = receiver_input_channel_3;
  center_channel_4 = receiver_input_channel_4;

  Serial.print("Center Channel 1: ");
  Serial.println(center_channel_1);
  Serial.print("Center Channel 2: ");
  Serial.println(center_channel_2);
  Serial.print("Center Channel 3: ");
  Serial.println(center_channel_3);
  Serial.print("Center Channel 4: ");
  Serial.println(center_channel_4);
  Serial.println();

  Serial.println("Mou el joystick de accelarar al màxim.");
  delay(5000);
  high_channel_3 = receiver_input_channel_3;
  Serial.print("High Channel 3: ");
  Serial.println(high_channel_3);
  Serial.println();

  Serial.println("Mou el joystick de accelarar al mínim.");
  delay(5000);
  low_channel_3 = receiver_input_channel_3;
  Serial.print("Low Channel 3: ");
  Serial.println(low_channel_3);
  Serial.println();

  Serial.println("Mou el joystick de roll el màxim a la dreta:");
  delay(5000);
  high_channel_1 = receiver_input_channel_1;
  Serial.print("High Channel 1: ");
  Serial.println(high_channel_1);
  Serial.println();

  Serial.println("Mou el joystick de roll el màxim a l'esquerra:");
  delay(5000);
  low_channel_1 = receiver_input_channel_1;
  Serial.print("Low Channel 1: ");
  Serial.println(low_channel_1);
  Serial.println();

  Serial.println("Mou el joystick de pitch el màxim amunt:");
  delay(5000);
  high_channel_2 = receiver_input_channel_2;
  Serial.print("High Channel 2: ");
  Serial.println(high_channel_2);
  Serial.println();

  Serial.println("Mou el joystick de pitch el màxim avall:");
  delay(5000);
  low_channel_2 = receiver_input_channel_2;
  Serial.print("Low Channel 2: ");
  Serial.println(low_channel_2);
  Serial.println();

  Serial.println("Mou el joystick de yaw el màxim a la dreta:");
  delay(5000);
  high_channel_4 = receiver_input_channel_4;
  Serial.print("High Channel 4: ");
  Serial.println(high_channel_4);
  Serial.println();

  Serial.println("Mou el joystick de yaw el màxim a l'esquerra:");
  delay(5000);
  low_channel_4 = receiver_input_channel_4;
  Serial.print("Low Channel 4: ");
  Serial.println(low_channel_4);
  Serial.println();


  Serial.println();
  Serial.println("Escrivint dades a la EEPROM...");
  EEPROM.write(0, center_channel_1 & 0b11111111);
  EEPROM.write(1, center_channel_1 >> 8);
  EEPROM.write(2, center_channel_2 & 0b11111111);
  EEPROM.write(3, center_channel_2 >> 8);
  EEPROM.write(4, center_channel_3 & 0b11111111);
  EEPROM.write(5, center_channel_3 >> 8);
  EEPROM.write(6, center_channel_4 & 0b11111111);
  EEPROM.write(7, center_channel_4 >> 8);
  EEPROM.write(8, high_channel_1 & 0b11111111);
  EEPROM.write(9, high_channel_1 >> 8);
  EEPROM.write(10, high_channel_2 & 0b11111111);
  EEPROM.write(11, high_channel_2 >> 8);
  EEPROM.write(12, high_channel_3 & 0b11111111);
  EEPROM.write(13, high_channel_3 >> 8);
  EEPROM.write(14, high_channel_4 & 0b11111111);
  EEPROM.write(15, high_channel_4 >> 8);
  EEPROM.write(16, low_channel_1 & 0b11111111);
  EEPROM.write(17, low_channel_1 >> 8);
  EEPROM.write(18, low_channel_2 & 0b11111111);
  EEPROM.write(19, low_channel_2 >> 8);
  EEPROM.write(20, low_channel_3 & 0b11111111);
  EEPROM.write(21, low_channel_3 >> 8);
  EEPROM.write(22, low_channel_4 & 0b11111111);
  EEPROM.write(23, low_channel_4 >> 8);
  Serial.println("Dades escrites. Verificant...");

  bool error = 0;
  if(center_channel_1 != ((EEPROM.read(1) << 8) | EEPROM.read(0)))error = 1;
  if(center_channel_2 != ((EEPROM.read(3) << 8) | EEPROM.read(2)))error = 1;
  if(center_channel_3 != ((EEPROM.read(5) << 8) | EEPROM.read(4)))error = 1;
  if(center_channel_4 != ((EEPROM.read(7) << 8) | EEPROM.read(6)))error = 1;
  
  if(high_channel_1 != ((EEPROM.read(9) << 8) | EEPROM.read(8)))error = 1;
  if(high_channel_2 != ((EEPROM.read(11) << 8) | EEPROM.read(10)))error = 1;
  if(high_channel_3 != ((EEPROM.read(13) << 8) | EEPROM.read(12)))error = 1;
  if(high_channel_4 != ((EEPROM.read(15) << 8) | EEPROM.read(14)))error = 1;
  
  if(low_channel_1 != ((EEPROM.read(17) << 8) | EEPROM.read(16)))error = 1;
  if(low_channel_2 != ((EEPROM.read(19) << 8) | EEPROM.read(18)))error = 1;
  if(low_channel_3 != ((EEPROM.read(21) << 8) | EEPROM.read(20)))error = 1;
  if(low_channel_4 != ((EEPROM.read(23) << 8) | EEPROM.read(22)))error = 1;

  if (error) {
    Serial.println("ERROR al verificar les dades escrites al EEPROM.");
  } else {
    Serial.println("Dades verificades!");
  }

  Serial.println();
  Serial.println("SETUP Finalitzat.");
}

void loop() {
  // put your main code here, to run repeatedly:

}

ISR(PCINT0_vect) {
  current_time = micros();
  
  // Canal 1
  if(PINB & B00000001){                                                     // Si el pin D8 - HIGH
    if(last_channel_1 == 0){                                                // Si abans el pin D8 - LOW
      last_channel_1 = 1;                                                   // Guardem valor actual del pin D8.
      timer_1 = current_time;                                               // Guardem current_time a timer_1.
    }
  }
  else if(last_channel_1 == 1){                                             // Altrament si el pin D8 - LOW i abans estava HIGH
    last_channel_1 = 0;                                                     // Guardem valor actual del pin D8.
    receiver_input_channel_1 = current_time - timer_1;                      // Guardem valor senyal del canal 1.
  }
  
  // Canal 2
  if(PINB & B00000010){                                                     // Si el pin D9 - HIGH
    if(last_channel_2 == 0){                                                // Si abans el pin D9 - LOW
      last_channel_2 = 1;                                                   // Guardem valor actual del pin D9.
      timer_2 = current_time;                                               // Guardem current_time a timer_2.
    }
  }
  else if(last_channel_2 == 1){                                             // Altrament si el pin D9 - LOW i abans estava HIGH
    last_channel_2 = 0;                                                     // Guardem valor actual del pin D9.
    receiver_input_channel_2 = current_time - timer_2;                      // Guardem valor senyal del canal 2.
  }
  
  // Canal 3
  if(PINB & B00000100){                                                     // Si el pin D10 - HIGH
    if(last_channel_3 == 0){                                                // Si abans el pin D10 - LOW
      last_channel_3 = 1;                                                   // Guardem valor actual del pin D10.
      timer_3 = current_time;                                               // Guardem current_time a timer_3.
    }
  }
  else if(last_channel_3 == 1){                                             // Altrament si el pin D9 - LOW i abans estava HIGH
    last_channel_3 = 0;                                                     // Guardem valor actual del pin D9.
    receiver_input_channel_3 = current_time - timer_3;                      // Guardem valor senyal del canal 3.
  }
  
  // Canal 4
  if(PINB & B00001000){                                                     // Si el pin D11 - HIGH
    if(last_channel_4 == 0){                                                // Si abans el pin D11 - LOW
      last_channel_4 = 1;                                                   // Guardem valor actual del pin D11.
      timer_4 = current_time;                                               // Guardem current_time a timer_4.
    }
  }
  else if(last_channel_4 == 1){                                             // Altrament si el pin D11 - LOW i abans estava HIGH
    last_channel_4 = 0;                                                     // Guardem valor actual del pin D11.
    receiver_input_channel_4 = current_time - timer_4;                      // Guardem valor senyal del canal 4.
  }
}
