#include <EEPROM.h>
#include "Wire.h"

///////////////////////////////////////////////////////////////////////////////////////
// CONSTANTS
///////////////////////////////////////////////////////////////////////////////////////
const int CALIB_GYRO_LOOPS = 2000, RED = 12, GREEN = 13;
const uint8_t MPU6050_ADDR  = 0x68;
const uint8_t ACCEL_XOUT_H   = 0x3B;

///////////////////////////////////////////////////////////////////////////////////////
// PID - CONSTANTS
///////////////////////////////////////////////////////////////////////////////////////
float pid_p_gain_pitch = 1.3;               // Constant P per pitch.
float pid_i_gain_pitch = 0.04;              // Constant I per pitch.
float pid_d_gain_pitch = 18.0;              // Constant D per pitch.
int pid_max_pitch = 400;                    // Màxim resultat del PID.

float pid_p_gain_roll = pid_p_gain_pitch;   // Constant P per pitch.
float pid_i_gain_roll = pid_i_gain_pitch;   // Constant I per pitch.
float pid_d_gain_roll = pid_d_gain_pitch;   // Constant D per pitch.
int pid_max_roll = pid_max_pitch;           // Màxim resultat del PID.

float pid_p_gain_yaw = 4.0;                 // Constant P per pitch.
float pid_i_gain_yaw = 0.02;                // Constant I per pitch.
float pid_d_gain_yaw = 0.0;                 // Constant D per pitch.
int pid_max_yaw = 400;                      // Màxim resultat del PID.

///////////////////////////////////////////////////////////////////////////////////////
// PID - VARIABLES
///////////////////////////////////////////////////////////////////////////////////////
float pid_error_temp;
float pitch_level_adjust, gyro_pitch_input;
float pid_pitch_setpoint, pid_i_mem_pitch, pid_output_pitch, pid_last_pitch_d_error;
float roll_level_adjust, gyro_roll_input;
float pid_roll_setpoint, pid_i_mem_roll, pid_output_roll, pid_last_roll_d_error;
float yaw_level_adjust, gyro_yaw_input;
float pid_yaw_setpoint, pid_i_mem_yaw, pid_output_yaw, pid_last_yaw_d_error;

///////////////////////////////////////////////////////////////////////////////////////
// IMU - VARIABLES
///////////////////////////////////////////////////////////////////////////////////////
int gyro_data_x, gyro_data_y, gyro_data_z;
double gyro_cal_x, gyro_cal_y, gyro_cal_z;
float angle_pitch_gyr = 0.0, angle_roll_gyr = 0.0;
long acc_data_x, acc_data_y, acc_data_z, acc_total_vector;
float angle_pitch_acc = 0.0, angle_roll_acc = 0.0;
float angle_pitch_output = 0.0, angle_roll_output = 0.0;
bool set_gyro_angles = false;
int angle_pitch_buffer, angle_roll_buffer;

///////////////////////////////////////////////////////////////////////////////////////
// RC - VARIABLES
///////////////////////////////////////////////////////////////////////////////////////
unsigned long current_time;
unsigned long last_channel_1, last_channel_2, last_channel_3, last_channel_4;
unsigned long timer_1, timer_2, timer_3, timer_4;
unsigned long receiver_input_channel_1, receiver_input_channel_2, receiver_input_channel_3, receiver_input_channel_4;
long throttle, receiver_roll, receiver_pitch, receiver_yaw;
unsigned long timer_esc_1, timer_esc_2, timer_esc_3, timer_esc_4;
unsigned long esc_1, esc_2, esc_3, esc_4;
unsigned long esc_timer, esc_loop_timer;

///////////////////////////////////////////////////////////////////////////////////////
// COMMON - VARIABLES
///////////////////////////////////////////////////////////////////////////////////////
int loop_counter;
int start, temperature;
unsigned long zero_timer;
byte eeprom_data[36];
float battery_voltage;

void setup() {
  Wire.begin();                                      // Comencem la comunicació I2C com a "master".
  TWBR = 12;                                         // Modifiquem la velocitat de rellotge del I2C a 400kHz.
  
  // Serial @ 57600bps 
  Serial.begin(57600);      
  Serial.println("Controller"); 
  Serial.println("");
  
  PCICR |= (1 << PCIE0);                              // Activem les interrupcions per el registre PCMSK0.
  PCMSK0 |= (1 << PCINT0);                            // Activem el pin digital 8 perque dispari la interrupco al canviar de valor.
  PCMSK0 |= (1 << PCINT1);                            // Activem el pin digital 9 perque dispari la interrupco al canviar de valor.
  PCMSK0 |= (1 << PCINT2);                            // Activem el pin digital 10 perque dispari la interrupco al canviar de valor.
  PCMSK0 |= (1 << PCINT3);                            // Activem el pin digital 11 perque dispari la interrupco al canviar de valor.
  
  DDRD |= B11111000;                                  // Configurem pins 3 (oscil·loscopi), 4, 5, 6, 7 (ESC) com output.
  DDRB |= B00110000;                                  // Configurem pins 12, 13 (leds) com output.
  PORTB &= B11001111;                                 // Apaguem leds.

  // Llegim la memòria EEPROM per accés més ràpid.
  for(int i = 0; i <= 35; i++) eeprom_data[i] = EEPROM.read(i);
  
  Serial.println("Setup MPU-6050...");
  setup_IMU();
  Serial.println("Setup done !");

  digitalWrite(RED, HIGH);
  for (int i = 0; i < 1250 ; i++){                     //Wait 5 seconds before continuing.
    PORTD |= B11110000;                                //Set digital poort 4, 5, 6 and 7 high.
    delayMicroseconds(1000);                           //Wait 1000us.
    PORTD &= B00001111;                                //Set digital poort 4, 5, 6 and 7 low.
    delayMicroseconds(3000);                           //Wait 3000us.
  }
  digitalWrite(RED, LOW);

  // Calibrate gyro.
  Serial.print("Calibrating");
  calibrate_gyro();
  Serial.println("OK");
  Serial.println("Finish setup");
  
  set_gyro_angles = false;
  loop_counter = 0;
  start = 0;
  zero_timer = micros();
}

void setup_IMU() {
  uint8_t gyro_address = 0x68;
  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x6B);                                           // Volem escriure al registre PWR_MGMT_1 (0x6B).
  Wire.write(0x00);                                           // Escrivim 00000000 al registre per activar el sensor.
  Wire.endTransmission();                                     // Fi de la comuniació.

  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1B);                                           // Volem escriure al registre GYRO_CONFIG (0x1B). 
  Wire.write(0x08);                                           // Escrivim 00001000 al registre per definir la sensibilitat a 500dps.
  Wire.endTransmission();                                     // Fi de la comuniació.

  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1C);                                           // Volem escriure al registre ACCEL_CONFIG (0x1C). 
  Wire.write(0x10);                                           // Escrivim 00010000 al registre per definir la sensibilitat a +/- 8g.
  Wire.endTransmission();                                     // Fi de la comuniació.

  // Comprovem que les dades s'han guardat correctament.
  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1B);                                           // Volem llegir el registre GYRO_CONFIG (0x1B).
  Wire.endTransmission();                                     // Fi de la comuniació.
  Wire.requestFrom(gyro_address, 1);                          // Demanem 1 byte al sensor.
  while(Wire.available() < 1);                                // Esperem a que tinguem les dades per llegir.
  
  if(Wire.read() != 0x08){                                    // Comprovem que el valor sigui el que hem escrit anteriorment.
    Serial.println("GYRO_CONFIG INCORRECT");
    while(1) delay(10);
  }

  Wire.beginTransmission(gyro_address);                       // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(0x1A);                                           // Volem escriure al registre CONFIG (0x1A). 
  Wire.write(0x03);                                           // Escrivim 00000011 al registre per definir el Digital Low Filter.
  Wire.endTransmission();                                     // Fi de la comuniació.
}

void calibrate_gyro() {
  digitalWrite(RED, HIGH);                                                    // Encenem el led vermell.
  for (int i = 0; i < CALIB_GYRO_LOOPS; i++) {
    if (i % 125 == 0) {
      digitalWrite(RED, !digitalRead(RED));                                   // Alternem el led vermell per mostrar que estem calibrant.
      Serial.print(".");
    }
    
    read_IMU_data(false);
    gyro_cal_x += gyro_data_x;
    gyro_cal_y += gyro_data_y;
    gyro_cal_z += gyro_data_z;

    // Per tal que els ESC no pitin enviem un pols de 1ms.
    PORTD |= B11000000;                                                     // Pin D6, D7 - HIGH.
    delayMicroseconds(1000);                                                // Esperem 1ms.
    PORTD &= B00111111;                                                     // Pin D6, D7 - LOW.
    delayMicroseconds(3000);                                                // Esperem 4ms.
  }
  
  gyro_cal_x /= CALIB_GYRO_LOOPS;
  gyro_cal_y /= CALIB_GYRO_LOOPS;
  gyro_cal_z /= CALIB_GYRO_LOOPS;
  
  digitalWrite(RED, LOW);                                                     // Apaguem el led vermell.
}

void show_battery_state() {
  if (battery_voltage > 11.1) {
    // Només led verd.
    PORTB |= B00100000;
    PORTB &= B11101111;
  }
  else if (battery_voltage > 9.00) {
    // Leds verd i vermell.
    PORTB |= B00110000;
  }
  else {
    // Només led vermell.
    PORTB |= B00010000;
    PORTB &= B11011111;
  }
}

void read_battery_voltage(bool filter) {
  float ar = analogRead(A0);
  float voltage;
  if (ar > 0) {
    // (5.00 / 1023.00) = 0.0048875855
    voltage = ar * 0.0048875855 * 3.2;
  }
  else voltage = 0;

  if (filter) battery_voltage = voltage * 0.7 + battery_voltage * 0.3;
  else battery_voltage = voltage;
}

void read_IMU_data(boolean calibrated) {
  Wire.beginTransmission(MPU6050_ADDR);                               // Comencem la comunicació amb el sensor MPU6050.
  Wire.write(ACCEL_XOUT_H);                                           // Volem llegir el registre ACCEL_XOUT_H (0x3B).
  Wire.endTransmission();                                             // Fi de la comuniació.

  Wire.requestFrom(MPU6050_ADDR, 14);                                 // Demanem 14 bytes al sensor.
  while (Wire.available() < 14);                                      // Esperem a que tinguem les dades per llegir.
  
  acc_data_x = (Wire.read() << 8) | Wire.read();
  acc_data_y = (Wire.read() << 8) | Wire.read(); 
  acc_data_z = (Wire.read() << 8) | Wire.read();
  temperature = (Wire.read() << 8) | Wire.read();
  gyro_data_x = (Wire.read() << 8) | Wire.read(); 
  gyro_data_y = (Wire.read() << 8) | Wire.read(); 
  gyro_data_z = (Wire.read() << 8) | Wire.read(); 

  if (calibrated) {
    // Restem les dades de calibració.
    gyro_data_x -= gyro_cal_x;
    gyro_data_y -= gyro_cal_y;
    gyro_data_z -= gyro_cal_z;
  }
}

void calculate_angles() {
  // Càlcul d'angles giroscopi.
  // 0.0000611 = 1 / 250Hz / 65.5.
  angle_pitch_gyr += gyro_data_y * 0.0000611;                                   // Calculem l'angle pitch rotat i el sumem a l'angle_pitch per saber l'orientació.
  angle_roll_gyr += gyro_data_x * 0.0000611;                                    // Calculem l'angle roll rotat i el sumem a l'angle_roll per saber l'orientació.

  //0.000001066 = 0.0000611 * (3.142(PI) / 180degr) La funció sin és en radiants.
  angle_pitch_gyr -= angle_roll_gyr * sin(gyro_data_z * 0.000001066);           // Si la IMU ha rotat en yaw transferim l'angle roll a l'angle pitch.
  angle_roll_gyr += angle_pitch_gyr * sin(gyro_data_z * 0.000001066);           // Si la IMU ha rotat transferim l'angle pitch a l'angle roll.

  // Càlcul d'angles acceleròmetre.
  acc_total_vector = sqrt((acc_data_x*acc_data_x)+(acc_data_y*acc_data_y)+(acc_data_z*acc_data_z));   // Calculem el vector total d'acceleració.
  // 57.296 = 1 / (3.142 / 180) La funció sin és en radiants.
  angle_pitch_acc = asin((float)acc_data_y/acc_total_vector)* 57.296;                                 // Calculem l'angle pitch.
  angle_roll_acc = asin((float)acc_data_x/acc_total_vector)* -57.296;                                 // Calculem l'angle roll.

  angle_roll_gyr = angle_roll_gyr * 0.9996 + angle_roll_acc * 0.0004;             // Corregim el drift de l'angle roll del giroscopi amb l'angle roll de l'accelerometre.
  angle_pitch_gyr = angle_pitch_gyr * 0.9996 + angle_pitch_acc * 0.0004;          // Corregim el drift de l'angle pitch del giroscopi amb l'angle pitch de l'accelerometre.
  
  angle_roll_output = angle_roll_gyr;                                             // Assignem angle roll.
  angle_pitch_output = angle_pitch_gyr;                                           // Assignem angle pitch.
}

void calculate_pid_setpoints() {
  gyro_roll_input = (gyro_roll_input * 0.7) + ((gyro_data_x / 65.5) * 0.3);         // Roll input en dps aplicant un filtre complementari per reduir soroll.
  gyro_pitch_input = (gyro_pitch_input * 0.7) + ((gyro_data_y / 65.5) * 0.3);       // Pitch input en dps aplicant un filtre complementari per reduir soroll.
  gyro_yaw_input = (gyro_yaw_input * 0.7) + ((gyro_data_z / 65.5) * 0.3);           // Yaw input en dps aplicant un filtre complementari per reduir soroll.
  
  roll_level_adjust = angle_roll_output * 15;                                       // Calculem l'ajustament per compensar la inclinació en roll.
  pitch_level_adjust = angle_pitch_output * 15;                                     // Calculem l'ajustament per compensar la inclinació en pitch.
  
  // El setpoint del PID estarà en dps ja que volem que depengui del pitch rebut del comandament RC.
  // Dividim per 3.0 per ajustar que la velocitat màxima de rotació quan posem el joystick a un extrem sigui de 164dps ((500-8)/3 = 164dps).
  // Si volguessim poder girar més ràpid o més lent hauriem de modficar aquest 3.
  pid_pitch_setpoint = 0;
  if (receiver_pitch > 1508) pid_pitch_setpoint = receiver_pitch - 1508;
  else if (receiver_pitch < 1492) pid_pitch_setpoint = receiver_pitch - 1492;
  pid_pitch_setpoint -= pitch_level_adjust;                                         // Restem l'ajustament de l'angle al setpoint.
  pid_pitch_setpoint /= 3.0;                                                        // Dividim per 3 per ajustar la velocitat angular màxima.

  pid_roll_setpoint = 0;
  if (receiver_roll > 1508) pid_roll_setpoint = receiver_roll - 1508;
  else if (receiver_roll < 1492) pid_roll_setpoint = receiver_roll - 1492;
  pid_roll_setpoint -= roll_level_adjust;
  pid_roll_setpoint /= 3.0;

  pid_yaw_setpoint = 0;
  if (throttle > 1050) {                                                            // Quan estiguem engegant o parant els motors no volem que el quadcopter faci rotació en yaw.
    if (receiver_yaw > 1508) pid_yaw_setpoint = receiver_yaw - 1508;
    else if (receiver_yaw < 1492) pid_yaw_setpoint = receiver_yaw - 1492;
    pid_yaw_setpoint /= 3.0;
  }
}

void calculate_pid() {
  // Calculs Roll.
  pid_error_temp = gyro_roll_input - pid_roll_setpoint;
  pid_i_mem_roll += pid_i_gain_roll * pid_error_temp;
  if (pid_i_mem_roll > pid_max_roll) pid_i_mem_roll = pid_max_roll;
  else if (pid_i_mem_roll < pid_max_roll * -1) pid_i_mem_roll = pid_max_roll * -1;

  pid_output_roll = pid_p_gain_roll * pid_error_temp + pid_i_mem_roll + pid_d_gain_roll * (pid_error_temp - pid_last_roll_d_error);
  if (pid_output_roll > pid_max_roll) pid_output_roll = pid_max_roll;
  else if (pid_output_roll < pid_max_roll * -1) pid_output_roll = pid_max_roll * -1;

  pid_last_roll_d_error = pid_error_temp;

  // Calculs Pitch.
  pid_error_temp = gyro_pitch_input - pid_pitch_setpoint;
  pid_i_mem_pitch += pid_i_gain_pitch * pid_error_temp;
  if (pid_i_mem_pitch > pid_max_pitch) pid_i_mem_pitch = pid_max_pitch;
  else if (pid_i_mem_pitch < pid_max_pitch * -1) pid_i_mem_pitch = pid_max_pitch * -1;

  pid_output_pitch = pid_p_gain_pitch * pid_error_temp + pid_i_mem_pitch + pid_d_gain_pitch * (pid_error_temp - pid_last_pitch_d_error);
  if (pid_output_pitch > pid_max_pitch) pid_output_pitch = pid_max_pitch;
  else if (pid_output_pitch < pid_max_pitch * -1) pid_output_pitch = pid_max_pitch * -1;

  pid_last_pitch_d_error = pid_error_temp;

  // Calculs Yaw.
  pid_error_temp = gyro_yaw_input - pid_yaw_setpoint;
  pid_i_mem_yaw += pid_i_gain_yaw * pid_error_temp;
  if (pid_i_mem_yaw > pid_max_yaw) pid_i_mem_yaw = pid_max_yaw;
  else if (pid_i_mem_yaw < pid_max_yaw * -1) pid_i_mem_yaw = pid_max_yaw * -1;

  pid_output_yaw = pid_p_gain_yaw * pid_error_temp + pid_i_mem_yaw + pid_d_gain_yaw * (pid_error_temp - pid_last_yaw_d_error);
  if (pid_output_yaw > pid_max_yaw) pid_output_yaw = pid_max_yaw;
  else if (pid_output_yaw < pid_max_yaw * -1) pid_output_yaw = pid_max_yaw * -1;

  pid_last_yaw_d_error = pid_error_temp;
}

void set_esc_values() {
  if (start == 2) {
    if (throttle > 1600) throttle = 1600;                                             // Limitem l'accelerador per tenir marge amb el PID i la compensació de bateria.
    esc_1 = throttle + pid_output_pitch + pid_output_roll + pid_output_yaw;           // Calculem el pols per l'ESC 1.
    esc_2 = throttle - pid_output_pitch + pid_output_roll - pid_output_yaw;           // Calculem el pols per l'ESC 2.
    esc_3 = throttle - pid_output_pitch - pid_output_roll + pid_output_yaw;           // Calculem el pols per l'ESC 3.
    esc_4 = throttle + pid_output_pitch - pid_output_roll - pid_output_yaw;           // Calculem el pols per l'ESC 4.

    if (battery_voltage < 12.4 && battery_voltage > 9.00) {                           // Mentre hi hagi bateria.
      esc_1 += (12.4 - battery_voltage) * 55;                                         // Quan la bateria sigui de 9.00V sumarà 187 per compensar.
      esc_2 += (12.4 - battery_voltage) * 55;                                
      esc_3 += (12.4 - battery_voltage) * 55;                                
      esc_4 += (12.4 - battery_voltage) * 55;                                
    }
    
    if (esc_1 < 1100) esc_1 = 1100;
    if (esc_2 < 1100) esc_2 = 1100;
    if (esc_3 < 1100) esc_3 = 1100;
    if (esc_4 < 1100) esc_4 = 1100;
    
    if (esc_1 > 2000) esc_1 = 2000;
    if (esc_2 > 2000) esc_2 = 2000;
    if (esc_3 > 2000) esc_3 = 2000;
    if (esc_4 > 2000) esc_4 = 2000;
    
  } else {                                                                            // Si els motors no estan engegats enviem un pols de 1ms.
    esc_1 = 1000;
    esc_2 = 1000;
    esc_3 = 1000;
    esc_4 = 1000;
  } 
}

void check_start() {
  // Per engegar els motors: accelerador al mínim i yaw a l'esquerra (pas 1).
  if (throttle < 1050 && receiver_yaw < 1050) start = 1;
  
  // Quan el yaw torna a estar a la part central engeguem (pas 2).
  if (start == 1 && throttle < 1050 && receiver_yaw > 1450) {
    start = 2;
    digitalWrite(GREEN, HIGH);
    
    // Definim els angles amb els valors de l'acceleròmetre.
    angle_roll_gyr = angle_roll_acc;
    angle_pitch_gyr = angle_pitch_acc;
    angle_roll_output = angle_roll_acc;
    angle_pitch_output = angle_pitch_acc;

    // Reiniciem les variables del PID.
    pid_i_mem_roll = 0;
    pid_last_roll_d_error = 0;
    pid_i_mem_pitch = 0;
    pid_last_pitch_d_error = 0;
    pid_i_mem_yaw = 0;
    pid_last_yaw_d_error = 0;
  }

  // Si els motors estan engegats i posem l'accelerador al mínim i el yaw a la dreta els parem.
  if (start == 2 && throttle < 1050 && receiver_yaw > 1950) start = 0;
}

void convert_receiver_inputs() {
  receiver_roll = convert_receiver_channel(1);
  receiver_pitch = convert_receiver_channel(2);
  throttle = convert_receiver_channel(3);
  receiver_yaw = convert_receiver_channel(4);
}

void loop() {
  loop_counter++;
  
  // Mostrem el led verd pampallugant pes mostrar que el programa ha començat.
  if (start != 2 && loop_counter <= 75) {
    digitalWrite(GREEN, HIGH);
  } else if (start != 2 && loop_counter <= 125) {
    digitalWrite(GREEN, LOW);
  } else {
    loop_counter = 0;
  }

  convert_receiver_inputs();
  calculate_angles();    
  check_start();                                                             
  calculate_pid_setpoints();
  calculate_pid();
  read_battery_voltage(true);
  show_battery_state();
  set_esc_values();    
  
  PORTD &= B11110111;                                           // Posem el pin D3 a LOW per l'oscil·loscopi.

  // Encenem el led vermell si el nostre loop excedeix els 4050 microsegons.
  if(micros() - zero_timer > 4050) digitalWrite(RED, HIGH);

  // Loop de 250Hz (4ms).
  while (zero_timer + 4000 > micros());
  zero_timer = micros();
  
  PORTD |= B00001000;                                            // Posem el pin D3 a HIGH per l'oscil·loscopi.
  PORTD |= B11110000;                                            // Posem els pins D4, D5, D6, D7 a HIGH per començar a enviar la senyal.
  timer_esc_1 = esc_1 + zero_timer;                              // Calculem el temps final per l'ESC 1.
  timer_esc_2 = esc_2 + zero_timer;                              // Calculem el temps final per l'ESC 2.
  timer_esc_3 = esc_3 + zero_timer;                              // Calculem el temps final per l'ESC 3.
  timer_esc_4 = esc_4 + zero_timer;                              // Calculem el temps final per l'ESC 4.

  // Tenim sempre almenys 1ms on no fem res. Aprofitem-ho.
  read_IMU_data(true);                                           // Llegim les dades de la IMU.

  while (PORTD >= 16) {                                          // Mentre els ports D4, D5, D6 o D7 estiguin a HIGH.
    esc_loop_timer = micros();                                   // Comprovem el temps actual.
    if(timer_esc_1 <= esc_loop_timer)PORTD &= B11101111;         // Quan el temps del ESC 1 ha finalitzat: D4 - LOW.
    if(timer_esc_2 <= esc_loop_timer)PORTD &= B11011111;         // Quan el temps del ESC 2 ha finalitzat: D5 - LOW.
    if(timer_esc_3 <= esc_loop_timer)PORTD &= B10111111;         // Quan el temps del ESC 3 ha finalitzat: D6 - LOW.
    if(timer_esc_4 <= esc_loop_timer)PORTD &= B01111111;         // Quan el temps del ESC 4 ha finalitzat: D7 - LOW.
  }
}

int convert_receiver_channel(int channel){
  int low, center, high, actual;
  int difference;

  actual = receiver_input_channel_1;
  if (channel == 2) actual = receiver_input_channel_2;
  if (channel == 3) actual = receiver_input_channel_3;
  if (channel == 4) actual = receiver_input_channel_4;
                                        
  low = (eeprom_data[channel * 2 + 15] << 8) | eeprom_data[channel * 2 + 14];     // Guardem el valor mínim del canal que volem calibrar.
  center = (eeprom_data[channel * 2 - 1] << 8) | eeprom_data[channel * 2 - 2];    // Guardem el valor mig del canal que volem calibrar.
  high = (eeprom_data[channel * 2 + 7] << 8) | eeprom_data[channel * 2 + 6];      // Guardem el valor màxim del canal que volem calibrar.

  if (actual < center){                                                           // El valor actual és més petit que el mig.
    if (actual < low) actual = low;                                               // Si és més petit que el mínim guardem el màxim.
    difference = ((long)(center - actual) * (long)500) / (center - low);          // Projectem el valor a 1000 - 2000 microsegons.
    return 1500 - difference;                                                     
  }
  else if (actual > center){                                                      // El valor actual és més gran que el mig.
    if (actual > high) actual = high;                                             // Si és més gran que el màxim guardem el mínim.
    difference = ((long)(actual - center) * (long)500) / (high - center);         // Projectem el valor a 1000 - 2000 microsegons.
    return 1500 + difference;                                                     
  }
  else return 1500;                                                               // Retornem el punt mig.
}

ISR(PCINT0_vect) {
  current_time = micros();
  
  // Canal 1
  if(PINB & B00000001){                                                     // Si el pin D8 - HIGH
    if(last_channel_1 == 0){                                                // Si abans el pin D8 - LOW
      last_channel_1 = 1;                                                   // Guardem valor actual del pin D8.
      timer_1 = current_time;                                               // Guardem current_time a timer_1.
    }
  }
  else if(last_channel_1 == 1){                                             // Altrament si el pin D8 - LOW i abans estava HIGH
    last_channel_1 = 0;                                                     // Guardem valor actual del pin D8.
    receiver_input_channel_1 = current_time - timer_1;                      // Guardem valor senyal del canal 1.
  }
  
  // Canal 2
  if(PINB & B00000010){                                                     // Si el pin D9 - HIGH
    if(last_channel_2 == 0){                                                // Si abans el pin D9 - LOW
      last_channel_2 = 1;                                                   // Guardem valor actual del pin D9.
      timer_2 = current_time;                                               // Guardem current_time a timer_2.
    }
  }
  else if(last_channel_2 == 1){                                             // Altrament si el pin D9 - LOW i abans estava HIGH
    last_channel_2 = 0;                                                     // Guardem valor actual del pin D9.
    receiver_input_channel_2 = current_time - timer_2;                      // Guardem valor senyal del canal 2.
  }
  
  // Canal 3
  if(PINB & B00000100){                                                     // Si el pin D10 - HIGH
    if(last_channel_3 == 0){                                                // Si abans el pin D10 - LOW
      last_channel_3 = 1;                                                   // Guardem valor actual del pin D10.
      timer_3 = current_time;                                               // Guardem current_time a timer_3.
    }
  }
  else if(last_channel_3 == 1){                                             // Altrament si el pin D10 - LOW i abans estava HIGH
    last_channel_3 = 0;                                                     // Guardem valor actual del pin D10.
    receiver_input_channel_3 = current_time - timer_3;                      // Guardem valor senyal del canal 3.
  }
  
  // Canal 4
  if(PINB & B00001000){                                                     // Si el pin D11 - HIGH
    if(last_channel_4 == 0){                                                // Si abans el pin D11 - LOW
      last_channel_4 = 1;                                                   // Guardem valor actual del pin D11.
      timer_4 = current_time;                                               // Guardem current_time a timer_4.
    }
  }
  else if(last_channel_4 == 1){                                             // Altrament si el pin D11 - LOW i abans estava HIGH
    last_channel_4 = 0;                                                     // Guardem valor actual del pin D11.
    receiver_input_channel_4 = current_time - timer_4;                      // Guardem valor senyal del canal 4.
  }
}
